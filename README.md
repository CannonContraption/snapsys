# SNAPsys

A small shell script for rotating daily backups

Defaults to keeping 7 days of backups.

# Setup

Change the variables at the top of the script, copy it into /sbin, and schedule it as a cron job.
